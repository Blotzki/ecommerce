import {Nav, Navbar, Container, Button, Form} from 'react-bootstrap';
import {NavLink, Link, useNavigate} from 'react-router-dom';
import {useContext, Fragment, useState, useEffect} from 'react'
import Logout from '../pages/Logout.js'
import AdminDashBoardRegister from '../pages/AdminDashBoardRegister.js'
import AdminDashBoardProduct from '../pages/AdminDashBoardProduct.js'
import UserContext from '../UserContext.js'
import Swal from 'sweetalert2'


export default function AppNavBar(){
	const {user, setUser} = useContext(UserContext);
	const [isAdmin, setIsAdmin] = useState(false)
	const navigate = useNavigate();
	const retrieveUserDetails = (token) =>{
		fetch(`${process.env.REACT_APP_API_URL}/user/profile`, {
			headers:{
				Authorization: `Bearer ${token}`
			}
		}).then(result=> result.json()).then(data=>{
			console.log(data);

			setUser({
				id:data._id,
				isAdmin:data.isAdmin
			});
		})
 	}
 	console.log(user)
	return(
		<Navbar bg="dark" variant="dark" expand="lg">
		     <Container fluid>
		       <Navbar.Brand as ={NavLink} to="/">dbExpress</Navbar.Brand>
		       <Navbar.Toggle aria-controls="basic-navbar-nav" />
		       <Navbar.Collapse id="basic-navbar-nav">
		         <Nav className="mx-auto navigation">
		           {
		           	user?
		           		user.isAdmin?
		           		<Fragment>
		           			<Nav.Link as ={NavLink} to="/adminDashBoardProduct"className="mx-3">Products</Nav.Link>
		           			<Nav.Link as ={NavLink} to="/adminDashBoardRegister"className="mx-3">Add Product</Nav.Link>
		           			<Nav.Link as ={NavLink} to="/logout" className="mx-3">Logout</Nav.Link>
		           		</Fragment>
		           :
		           <Fragment>
		           <Nav.Link as ={NavLink} to="/" className="mx-3">Home</Nav.Link>
		           <Nav.Link as ={NavLink} to="/userProductView" className="mx-3">Products</Nav.Link>
		           <Nav.Link as ={NavLink} to="/logout" className="mx-3">Logout</Nav.Link>
		           </Fragment>
		          :
		          <Fragment>
		           		<Nav.Link as ={NavLink} to="/login" >Login</Nav.Link>
		           		<Nav.Link as ={NavLink} to="/register" >Register</Nav.Link>
		           	</Fragment>

		           }
		         </Nav>
		         </Navbar.Collapse>
		     </Container>
		   </Navbar>
	)
}
