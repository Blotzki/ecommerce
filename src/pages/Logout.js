import UserContext from '../UserContext.js';
import {useContext, useEffect} from 'react';
import {Navigate} from 'react-router-dom';


export default function Logout(){

	const {unSetUser, setUser} = useContext(UserContext);
	
	useEffect(()=>{
		unSetUser();
		setUser(null);
	}, [])

	return(

		<Navigate to = "/login" />
		)
}