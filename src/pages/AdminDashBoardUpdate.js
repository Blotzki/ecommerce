import {useState, useEffect, Fragment, useContext} from 'react'
import {Form, Button, Image, Container, Col, Row, Card, Modal} from 'react-bootstrap'
import {useParams, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext.js';
import Swal from 'sweetalert2'


export default function AdminDashBoardUpdate(){

	const [category, setCategory] = useState("");
	const [productPhoto, setProductPhoto] = useState("");
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [brand, setBrand] = useState("");
	const [model, setModel] = useState("");
	const [regPrice, setRegPrice] = useState("");
	const [stocks, setStocks] = useState("");

	const [updateCategory, setUpdateCategory] = useState("");
	const [updateProductPhoto, setUpdateProductPhoto] = useState("");
	const [updateName, setUpdateName] = useState("");
	const [updateDescription, setUpdateDescription] = useState("");
	const [updateBrand, setUpdateBrand] = useState("");
	const [updateModel, setUpdateModel] = useState("");
	const [updateRegPrice, setUpdateRegPrice] = useState("");
	const [updateStocks, setUpdateStocks] = useState("");
	const [isActive, setIsActive] = useState("");
	const {user, setUser} = useContext(UserContext);

	const {productId} = useParams();
	const navigate = useNavigate();

	const [show, setShow] = useState(false);

  	const handleClose = () => setShow(false);
  	const handleShow = () => setShow(true);


	useEffect(()=>{

		fetch(`${process.env.REACT_APP_API_URL}/product/${productId}`).then(result=>result.json()).then(data=>{
			console.log(data)

			setCategory(data.category)
			setProductPhoto(data.productPhoto)
			setName(data.name)
			setDescription(data.description)
			setBrand(data.brand)
			setModel(data.model)
			setRegPrice(data.regPrice)
			setStocks(data.stocks)
			setIsActive(data.isActive)
			
		})
	})

	function deleteProduct(event){
		event.preventDefault();
		fetch(`${process.env.REACT_APP_API_URL}/product/${productId}/archive`,{
			method:"PUT",
			headers:{
				'Content-Type':'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}

		}).then(result=>result.json()).then(data=>{
			if(data){
				Swal.fire({
					title:"Product already archived!",
					icon:"success",
					text:"See other products!"
				})
				navigate("/adminDashBoardProduct");
				
			}else{
				Swal.fire({
					title:"Error!",
					icon:"error",
					text:"Please try again!"
				})
			}
		})

	}

	function restoreProduct(event){
		event.preventDefault();
		fetch(`${process.env.REACT_APP_API_URL}/product/${productId}/restore`,{
			method:"PUT",
			headers:{
				'Content-Type':'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}

		}).then(result=>result.json()).then(data=>{
			if(data){
				Swal.fire({
					title:"Product already restored!",
					icon:"success",
					text:"See other products!"
				})
				navigate("/adminDashBoardProduct");
				
			}else{
				Swal.fire({
					title:"Error!",
					icon:"error",
					text:"Please try again!"
				})
			}
		})

	}



	function updateProduct(event){
		event.preventDefault();
		fetch(`${process.env.REACT_APP_API_URL}/product/${productId}/update`,{
			method:"PUT",
			headers:{
				'Content-Type':'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body:JSON.stringify({
				productPhoto:updateProductPhoto,
				name: updateName,
				description: updateDescription,
				brand: updateBrand,
				model: updateModel,
				regPrice:updateRegPrice,
				stocks:updateStocks,
				category: updateCategory
			})
		}).then(result=> result.json()).then(data=>{
			if(data){
				Swal.fire({
					title:"Update Success!",
					icon:"success",
					text:"Check the updated product!"
				})
				navigate("/adminDashBoardProduct")
			}else{
				Swal.fire({
					title:"Error!",
					icon:"error",
					text:"Try again!"
				})
			}
		})
	}

	return(
		<Fragment>
		<h1 className="text-center mt-3"> Update Product</h1>
			<Container >
				<Row>
					<Col className="mt-5 mb-5 col-6">
						<Card style={{ width: '25rem' }}>
				      		<Card.Img className="img-fluid" variant="top" src={productPhoto} />
				      		<Card.Body>
				        		<Card.Title className="text-center">{name}</Card.Title>
						        <Card.Subtitle className="mt-3">Category: {category}</Card.Subtitle>
						        <Card.Subtitle className="mt-2">Brand: {brand}</Card.Subtitle>
						        <Card.Subtitle className="mt-2">Model: {model}</Card.Subtitle>
						        <Card.Subtitle className="mt-2">Price: Php {regPrice}</Card.Subtitle>
						        <Card.Subtitle className="mt-2">Stocks: {stocks}</Card.Subtitle>
						        <Card.Subtitle className="mt-2">Description</Card.Subtitle>
						        <p className="mt-2 p-card">{description}</p>
						    </Card.Body>
					      	{
					      		isActive?
					      	<Button className="mt-3 px-3" variant="danger" type="submit" onClick={handleShow} >
					       	  Delete 
					      	</Button>
					      :
					       <Button className="mt-3 px-3" variant="success" type="submit" onClick={handleShow} >
					       	  Restore 
					      </Button>
					  		}
				      <Modal show={show} onHide={handleClose}>
				             <Modal.Header closeButton>
				             { isActive?
				               <Modal.Title>Are you sure you want to archive {name}?</Modal.Title>
				               :
				               <Modal.Title>Are you sure you want to restore {name}?</Modal.Title>
				           }
				             </Modal.Header>
				             <Modal.Footer>
				               <Button variant="secondary" onClick={handleClose}>
				                 Back
				               </Button>
				               {
				               	isActive?
				               <Button variant="danger" 
				               	onClick={event => deleteProduct(event)}>
				               	Delete Product
				               	</Button>
				                :
				                <Button variant="success"
				                onClick={event => restoreProduct(event)}>
				                 Restore Product
				               </Button>
				           		}
				             </Modal.Footer>
				           </Modal>
				    	</Card>
					</Col>
						<Col className="col-6">
						    <Form className="mt-5 form mx-auto" onSubmit={event=>updateProduct(event)}>
							   
							    <Form.Group className="mb-3" controlId="formBasicFirstName">
							        <Form.Label className="text-primary">Product Photo URL:</Form.Label>
							        <Form.Control 
							        	type="text" 
							        	placeholder="Enter product photo url"
							        	value = {updateProductPhoto}
							        	onChange = {event => setUpdateProductPhoto(event.target.value)}
							        	required />
							    </Form.Group>

							    <Form.Group className="mb-3" controlId="formCategory">
							        <Form.Label className="text-primary">Category:</Form.Label>
							        <Form.Control 
							        	type="text" 
							        	placeholder="Enter Last Name"
							        	value = {updateCategory}
							        	onChange = {event => setUpdateCategory(event.target.value)}
							        	required />
							        	
							    </Form.Group>

							    <Form.Group className="mb-3" controlId="formName">
							        <Form.Label className="text-primary">Name:</Form.Label>
							        <Form.Control 
							        	type="text" 
							        	placeholder="Enter Product Name"
							        	value = {updateName}
							        	onChange = {event => setUpdateName(event.target.value)}
							        	required />
							    </Form.Group>

							    <Form.Group className="mb-3" controlId="formDescription">
							        <Form.Label className="text-primary">Description:</Form.Label>
							        <Form.Control 
							        	type="text" 
							        	placeholder="Product description"
							        	value = {updateDescription}
							        	onChange = {event => setUpdateDescription(event.target.value)}
							        	required />
							    </Form.Group>

							    <Form.Group className="mb-3" controlId="formBrand">
							        <Form.Label className="text-primary">Brand</Form.Label>
							        <Form.Control 
							        	type="text" 
							        	placeholder="Brand"
							        	value = {updateBrand}
							        	onChange ={event=> setUpdateBrand(event.target.value)} 
							        	required />
							    </Form.Group>

							    <Form.Group className="mb-3" controlId="formModel">
							        <Form.Label className="text-primary">Model</Form.Label>
							        <Form.Control 
							        	type="text" 
							        	placeholder="Product model" 
							        	value = {updateModel}
							        	onChange ={event=> setUpdateModel(event.target.value)}
							        	required />
							    </Form.Group>

							    <Form.Group className="mb-3" controlId="formStocks">
							        <Form.Label className="text-primary">Stocks</Form.Label>
							        <Form.Control 
							        	type="text" 
							        	placeholder="Enter product stocks" 
							        	value = {updateStocks}
							        	onChange ={event=> setUpdateStocks(event.target.value)}
							        	required />
							    </Form.Group>

							    <Form.Group className="mb-3" controlId="formRegPrice">
							        <Form.Label className="text-primary">Regular Price</Form.Label>
							        <Form.Control 
							        	type="text" 
							        	placeholder="Product regular price" 
							        	value = {updateRegPrice}
							        	onChange ={event=> setUpdateRegPrice(event.target.value)}
							        	required />
							    </Form.Group>
							    	<div className="d-grid gap-2 mt-4">
						            <Button variant="primary" type="submit" size="md">
						              Update
						            </Button>
						      		</div>
							</Form>
						</Col>
			    	</Row>
			    </Container>
			</Fragment>

			    
		)
}