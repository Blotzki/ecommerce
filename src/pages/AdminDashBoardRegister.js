import UserContext from '../UserContext.js'
import {useContext, useState} from 'react'
import {Form, Button} from 'react-bootstrap'
import {useNavigate} from 'react-router-dom'
import Swal from 'sweetalert2'

export default function AdminDashBoardRegister(){

		const [productPhoto, setProductPhoto] = useState('');
		const [category, setCategory] = useState('');
		const [name, setName] = useState('');
		const [description, setDescription] = useState('');
		const [brand, setBrand] = useState('');
		const [model, setModel] = useState('');
		const [regPrice, setRegPrice] = useState('');
		const [stocks, setStocks] = useState("");
		const {user} = useContext(UserContext);
				
		const navigate = useNavigate()

		function inputProduct(event){
					event.preventDefault();

					fetch(`${process.env.REACT_APP_API_URL}/product/inputProduct`, {
						method:"POST",
						headers:{
							'Content-Type':'application/json',
							Authorization: `Bearer ${localStorage.getItem('token')}`
						},
						body:JSON.stringify({
							addedBy:user._id,
							name: name,
							description: description,
							brand: brand,
							model: model,
							regPrice:regPrice,
							productPhoto:productPhoto,
							category:category,
							stocks:stocks
						})
					}).then(result=>result.json()).then(data=>{
						if(data){
							Swal.fire({
							title:"Product has been added!",
							icon:"success",
							text:"See Product"
						})

						navigate("/adminDashBoardProduct");
					}else{
						Swal.fire({
							title:"Registration Failed!",
							icon:"error",
							text:"Please try to again!"
						})
					}
					})

				}

		return(

			<Form className="mx-auto mt-3 registerForm" onSubmit={event=> inputProduct(event)}>
			    	<h1 className="text-center mt-3">Register</h1>
					 <Form.Group className="mb-3" controlId="formBasicFirstName">
			    		<Form.Label className="text-primary">Product Photo:</Form.Label>
			    		<Form.Control 
			    		type="text" 
			    		placeholder="Enter product photo url"
			    		value = {productPhoto}
			    		onChange = {event => setProductPhoto(event.target.value)}
			    		required />
			  		</Form.Group>

			    	<Form.Group className="mb-3" controlId="formCategory">
			    		<Form.Label className="text-primary">Category:</Form.Label>
			    		<Form.Control 
			    			type="text" 
			    			placeholder="Enter Last Name"
			    			value = {category}
			    			onChange = {event => setCategory(event.target.value)}
			    			required />
			    	</Form.Group>

			    	<Form.Group className="mb-3" controlId="formName">
			    		<Form.Label className="text-primary">Name:</Form.Label>
			    		<Form.Control 
			    			type="text" 
			    			placeholder="Enter Product Name"
			    			value = {name}
			    			onChange = {event => setName(event.target.value)}
			    			required />
			    	</Form.Group>

			    	<Form.Group className="mb-3" controlId="formDescription">
			    		<Form.Label className="text-primary">Description:</Form.Label>
			    		<Form.Control 
			    			type="text" 
			    			placeholder="Product description"
			    			value = {description}
			    			onChange = {event => setDescription(event.target.value)}
			    			required />
			    	</Form.Group>

			    	<Form.Group className="mb-3" controlId="formBrand">
			    		<Form.Label className="text-primary">Brand</Form.Label>
			    		<Form.Control 
			    			type="text" 
			    			placeholder="Brand"
			    			value = {brand}
			    			onChange ={event=> setBrand(event.target.value)} 
			    			required />
			    	</Form.Group>

			    	<Form.Group className="mb-3" controlId="formModel">
			    		<Form.Label className="text-primary">Model</Form.Label>
			    		<Form.Control 
			    			type="text" 
			    			placeholder="Product model" 
			    			value = {model}
			    			onChange ={event=> setModel(event.target.value)}
			    			required />
			    	</Form.Group>

			    	<Form.Group className="mb-3" controlId="formStocks">
			    		<Form.Label className="text-primary">Stocks</Form.Label>
			    		<Form.Control 
			    			type="text" 
			    			placeholder="Enter product stocks" 
			    			value = {stocks}
			    			onChange ={event=> setStocks(event.target.value)}
			    			required />
			    	</Form.Group>

			    	<Form.Group className="mb-3" controlId="formRegPrice">
			    		<Form.Label className="text-primary">Regular Price</Form.Label>
			    		<Form.Control 
			    			type="text" 
			    			placeholder="Producregular price" 
			    			value = {regPrice}
			    			onChange ={event=> setRegPrice(event.target.value)}
			    			required />
			    	</Form.Group>
			    	<Button variant="primary" type="submit">
			    			Submit
			    	</Button>
			</Form>
			    				


		)
	} 

