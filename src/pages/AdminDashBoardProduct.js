import {Fragment, useState, useEffect} from 'react'
import ProductCard from '../components/ProductCard.js'
import {Table} from 'react-bootstrap'


export default function AdminDashBoardProduct(){

const [products, setProducts] = useState([]);

	useEffect(()=>{
		fetch(`${process.env.REACT_APP_API_URL}/product/all`)
		.then(result => result.json())
		.then(data =>{
				setProducts(data.map( product => {
					return (
						
					<ProductCard key= {product._id} productProp = {product}/>
					
					)
				}))
		})
	}, [])

	return(

		<Fragment>

			<h1 className="text-center mb-4 mt-3">Products</h1>
			<Table striped bordered hover variant="dark" className="text-center mb-5">
				<thead>
					<tr className="text-center">
						<th>CATEGORY</th>
					    <th>IMAGE</th>
					    <th>NAME</th>
					    <th>DESCRIPTION</th>
					    <th>BRAND</th>
					    <th>MODEL</th>
					    <th>PRICE</th>
					    <th>STOCKS</th>
					    <th>STATUS</th>
					    <th></th>  
					</tr>
				</thead>
				<tbody>
					{products}
				</tbody>
			</Table>
		</Fragment>
		)
}