import {Fragment, useState, useEffect} from 'react'
import ProductCard from '../components/ProductCard.js'
import {Row, Col, Card, Button, Table, Carousel} from 'react-bootstrap'


export default function Home(){

		return(
			<Fragment>
				<Carousel>
				      <Carousel.Item>
				        <img
				          className="d-block w-100"
				          src="https://i.postimg.cc/KYLCxg2Q/Hero-slider-office-365-1920x700.jpg"
				          alt="First slide"
				        />
				      </Carousel.Item>
				      <Carousel.Item>
				        <img
				          className="d-block w-100"
				          src="https://i.postimg.cc/3NyXQ2Mx/1920-x-700-Intel-13th-gen-D21-T.jpg"
				          alt="Second slide"
				        />
				      </Carousel.Item>
				      <Carousel.Item>
				        <img
				          className="d-block w-100"
				          src="https://i.postimg.cc/KjrM2fqF/1920x700-Header-AMD-Productivity.jpg"
				          alt="Third slide"
				        />
				      </Carousel.Item>
				</Carousel>
			    <section className="container d-lg-flex mt-4 mb-5 p-2" id="links-below">
					<a href="#" className="text-dark d-block text-center">ABOUT US</a>
					<a href="#" className="text-dark d-block text-center">BULK ORDERS</a>
					<a href="#" className="text-dark d-block text-center">DELIVERY AREAS</a>
					<a href="#" className="text-dark d-block text-center">PAYMENT METHOD</a>
					<a href="#" className="text-dark d-block text-center">FAQs</a>
					<a href="#" className="text-dark d-block text-center">TERMS AND CONDITION</a>
				</section>
			</Fragment>


			)

}