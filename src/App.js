import AppNavBar from './components/AppNavBar.js';
import Login from './pages/Login.js';
import Logout from './pages/Logout.js';
import Home from './pages/Home.js';
import Register from './pages/Register.js';
import AdminDashBoardRegister from './pages/AdminDashBoardRegister.js';
import AdminDashBoardProduct from './pages/AdminDashBoardProduct.js';
import AdminDashBoardUpdate from './pages/AdminDashBoardUpdate.js';
import UserProductView from './pages/UserProductView.js';
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import {Fragment, useEffect, useState} from 'react'
import {UserProvider} from './UserContext.js'

import './App.css';

function App() {
  const [user, setUser] = useState(null);

  useEffect(()=>{
  }, [user]);

  const unSetUser = () =>{
    localStorage.clear();
  }

  useEffect(()=>{

    fetch(`${process.env.REACT_APP_API_URL}/user/profile`, {
        headers:{
          Authorization:`Bearer ${localStorage.getItem('token')}`
        }
    }).then(result => result.json()).then(data =>{

        if(localStorage.getItem('token') !== null){
             setUser({
              id:data._id,
              isAdmin: data.isAdmin});
        }else{
          setUser(null);
        }
       
      })

  }, [])


  return (
    <UserProvider value={{user, setUser, unSetUser}}>
          <Router>
            <AppNavBar/>
            <Routes>
              <Route path="/" element = {<Home/>} />
              <Route path="/login" element = {<Login/>} />
              <Route path="/adminDashBoardRegister" element = {<AdminDashBoardRegister/>} />
              <Route path="/adminDashBoardProduct" element = {<AdminDashBoardProduct/>} />
              <Route path="/userProductView" element = {<UserProductView/>} />
              <Route path="/adminDashBoardUpdate/:productId" element = {<AdminDashBoardUpdate/>} />
              <Route path="/logout" element = {<Logout />} />
              <Route path="/register" element = {<Register />} />
            </Routes>
          </Router>
        </UserProvider>
   
  );
}

export default App;
